package Modul3B;

import java.awt.Color;
import java.awt.Container;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextField;

class Ch14AbsolutePositioning_1 extends JFrame {
    private static final int FRAME_WIDTH     = 300;
    private static final int FRAME_HEIGTH    = 200;
    private static final int FRAME_X_ORIGIN  = 300;
    private static final int FRAME_Y_ORIGIN  = 100;
    private static final int BUTTON_WIDTH    = 80;
    private static final int BUTTON_HEIGHT   = 40;
    private JButton cancelButton;
    private JButton okButton;
    private JTextField tctField;
    
    public static void main(String[] args) {
         Ch14AbsolutePositioning_1 frame = new Ch14AbsolutePositioning_1();
         frame.setVisible(true);
    }
    
    public Ch14AbsolutePositioning_1() {
        Container contentPane = getContentPane();
        setSize      (FRAME_WIDTH, FRAME_HEIGTH);
        setResizable (true);
        setTitle     ("Program Ch14AbsolutePositioning");
        setLocation  (FRAME_X_ORIGIN, FRAME_Y_ORIGIN );
        
        contentPane.setLayout(null);
        contentPane.setBackground(Color.white);
        
        okButton = new JButton("OK");
        okButton.setBounds(100, 50, BUTTON_WIDTH, BUTTON_HEIGHT);
//        okButton.setBounds(110, 100, BUTTON_WIDTH, BUTTON_HEIGHT);
        contentPane.add(okButton);
        
        cancelButton = new JButton("CANCEL");
        cancelButton.setBounds(100, 100, BUTTON_WIDTH, BUTTON_HEIGHT);
//        cancelButton.setBounds(160, 125, BUTTON_WIDTH, BUTTON_HEIGHT);
        contentPane.add(cancelButton);
        
        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }
}
