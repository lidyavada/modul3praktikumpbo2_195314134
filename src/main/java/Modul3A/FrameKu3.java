package Modul3A;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

public class FrameKu3 extends JFrame {
    public FrameKu3() {
        this.setSize(300, 500);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setTitle("Ini Class turunan dari class JFrame");
        this.setVisible(true);
        
        JPanel panel = new JPanel(); 
        JButton tombol = new JButton();
        tombol.setText("Ini Tombol");
        panel.add(tombol);
        
        JLabel label = new JLabel("Labelku");
        panel.add(label); //menambahkan komponen label ke panel
        
        JTextField text = new JTextField();
        panel.add(text);
        
        JCheckBox check = new JCheckBox();
        panel.add(check);
        
        JRadioButton radio = new JRadioButton();
        panel.add(radio);
        this.add(panel);
    }
    public static void main(String[] args) {
        new FrameKu3();
    }
}