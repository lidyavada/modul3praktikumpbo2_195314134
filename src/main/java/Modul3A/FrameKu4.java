package Modul3A;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class FrameKu4 extends JFrame {
    public FrameKu4() {
        this.setLayout(null);
        this.setSize(200, 100);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setTitle("Find");
        this.setVisible(true);
        
        JLabel label = new JLabel("Keyword:");
        label.setBounds(50, 10, 120, 20);
        this.add(label);
        
        JTextField text = new JTextField();
        text.setBounds(150, 10, 150, 20);   
        this.add(text);
        
        JPanel panel = new JPanel();
        JButton tombol = new JButton("Find");
        this.add(tombol);
        tombol.setBounds(120, 50, 80, 20);
        this.add(panel);
    }
    public static void main(String[] args) {
        new FrameKu4();
    }
}