package Modul3A;

import javax.swing.JFrame;

public class Frameku1 {
    public static void main(String[] args) {
        JFrame frame = new JFrame ("Ini Frame Pertamaku");
        
        int tinggi = 400;
        int lebar = 400;
        
        frame.setBounds(0, 0, lebar, tinggi); //u/ mengatur posisi x y, lebar dan tinggi
//        mengatur auto close
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }
}
